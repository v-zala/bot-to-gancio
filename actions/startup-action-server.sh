#!/bin/sh
if [ -z "$PORT"]
then
  PORT=5055
fi

python -m rasa_sdk --actions actions -p $PORT -vv