# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
import requests, os

from dotenv import load_dotenv

load_dotenv()

class ActionAddEvent(Action):

    def name(self) -> Text:
        return "action_add_event"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        GANCIO_URL = os.getenv('GANCIO_URL')
        url = GANCIO_URL+ "/api/event"
        title = tracker.get_slot("title")
        start_datetime = tracker.get_slot("start_datetime")
        print(start_datetime)
        place_name = tracker.get_slot("place_name")

        querystring = {"title":"ejemplo"}

        payload = {
            "id": 217,
            "title": title,
            "slug": title,
            "multidate": False,
            "start_datetime": 1685527310,
            "end_datetime": None,
            "online_locations": ["https://prueba.com"],
            "updatedAt": "2023-05-25T11:04:17.089Z",
            "parentId": None,
            "place_name": place_name,
            "place_address": "null",
            "tags": ["tag"],
        }
        headers = {"Content-Type": "application/json"}

        response = requests.request("POST", url, json=payload, headers=headers, params=querystring)

        print(response.text)        

        dispatcher.utter_message(text="Evento subido, ahora falta que pase por moderación y se acepte. Gracias por subir el evento :)")

        return []
