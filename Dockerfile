# Extend the official Rasa image
FROM rasa/rasa:3.5.10-full

# Use subdirectory as working directory
WORKDIR /app

VOLUME /models
# ./:/app

# COPY rasa folder to working directory
COPY . /app

# Change back to root user to 
USER root

# Train a new model
RUN  rasa train --domain domain.yml --data data -c config.yml --endpoints endpoints.yml --out models -vv

# Set up models permision
RUN chmod 777 -R /app/models

# By best practices, don't run the code with root user
USER 1001

CMD ["run","--model","/app/models","--enable-api","--cors","*","--debug" ,"--endpoints", "endpoints.yml", "--log-file", "out.log", "--credentials", "credentials.yml"]

