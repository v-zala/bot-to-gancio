# BotToGancio

[![Build][build-shield]][build-url]
[![Issues][issues-shield]][issues-url]
[![AGPL License][license-shield]][license-url]
<!-- [![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url] -->

Ingesta de eventos desde un bot a una instancia de Gancio. Utiliza [Rasa Open Source](https://rasa.com/docs/rasa/) como framework de creación de agentes conversacionales. El escenario de desarrollo en local consta de una instancia de Rasa, Rasa SDK (servidor de acciones) y Gancio. Rasa sirve para obtener los parámetros necesarios de un evento mediante lenguaje natural. Rasa SDK se utiliza para ejecutar la petición de añadir el evento a la instancia de Gancio. Mientras que el Gancio se levantará para hacer las pruebas durante el desarrollo en local. 

**Tabla de Contenidos**
- [BotToGancio](#bottogancio)
  - [Requisitos](#requisitos)
  - [Instalación](#instalación)
    - [Variables de entorno](#variables-de-entorno)
    - [Docker](#docker)
    - [Manualmente](#manualmente)
  - [Integraciones](#integraciones)
    - [Telegram](#telegram)
  - [Exponer al exterior](#exponer-al-exterior)

## Requisitos

- Haber levantado una instancia de Gancio previamente. Se presupone que la red creada por docker es `gancio_default`.
- Entrenar el modelo.

## Instalación

### Variables de entorno

Copiamos las variables de entorno de ejemplo que iremos rellenando más adelante:

```zsh
cp .env.sample .env
cp actions/.env.sample actions/.env
```

### Docker

En primer lugar, es necesario entrenar el modelo. Para ello, ejecutamos:

```zsh
docker run -v $(pwd):/app rasa/rasa:3.5.10-full train --domain domain.yml --data data --out models
```

En segundo lugar, levantamos el servidor de acciones y el servicio de rasa en terminales distintas:

```zsh
docker run --net gancio_default -v $(pwd)/actions:/app/actions --name action-server-docker  -it $(docker build -q ./actions)
docker run -it --net gancio_default -v $(pwd):/app --name rasa-docker --env-file .env rasa/rasa:3.5.10-full shell
```

Ahora podemos probar el funcionamiento del bot en la consola.

También podemos ejecutar una consola interactiva donde ir entrenando el modelo:

```zsh
docker run -it --net gancio_default -v $(pwd):/app rasa/rasa:3.5.10-full interactive
```

Para levantar el escenario con Docker Compose, utilizamos el siguiente comando:

```zsh
docker compose up --build --force-recreate --remove-orphans
``` 

En el archivo `docker-compose.yml` podemos ver que la imagen del servidor de acciones se construye con el Dockerfile en la carpeta `actions`. Por otro lado, en el archivo `endpoints.yml` podemos comprobar que la dirección del servidor de acciones se corresponde con el nombre del contenedor levantado. Una vez ejecutado el comando anterior, esperamos a que el servidor de Rasa esté levantado y probamos a realizar una petición a la API de Rasa. Se recomienda utilizar la herramienta [Insomnia](https://insomnia.rest/) para las pruebas de las llamadas a las APIs. 

```zsh
curl -XPOST http://localhost:5005/webhooks/rest/webhook \
  -H "Content-type: application/json" \
  -d '{"sender": "test", "message": "Hola"}'
``` 

En caso de modificar los datos de Rasa o del servidor de acciones, paramos el escenario con `docker compose down` y eliminamos las imágenes con:
```zsh
docker compose rm -f 
```

### Manualmente

Actualmente [**Rasa sólo soporta hasta Python 3.10**](https://rasa.com/docs/rasa/installation/environment-set-up#1-python-environment-setup), por lo que es necesario instalar esa versión en caso de tener una versión superior de Python. Por ejemplo, en Debian se puede seguir [esta guía](https://computingforgeeks.com/how-to-install-python-on-debian-linux/).

Los siguientes paquetes son necesarios que estén preinstados antes de compilar python:

```
libbz2-dev build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev libsqlite3-dev wget libbz2-dev
```

Una vez instalado Python 3.10, se crea un entorno virtual de Python y lo activamos.

```zsh
python3.10 -m venv ./venv
source ./venv/bin/activate
```

Instalamos los requisitos en dicho entorno:

```zsh
python3.10 -m pip install -r requirements.txt
```

Entrenamos el modelo de Rasa:

```zsh
rasa train
```

Aquí veremos que podemos utilizar el archivo `endpoints.local.yml` con la dirección local del servidor de acciones. Otra opción es cambiar la url en el archivo de `endpoints.yml`, configurando la dirección del servidor de acciones de la siguiente manera:

```yaml
action_endpoint:
#  url: "http://action-server:5055/webhook"
 url: "http://localhost:5055/webhook"
``` 

Levantamos el servidor de acciones de rasa y el bot de rasa en terminales distintas:

```zsh
rasa run actions --port 5055;
# Abrimos otra terminal
rasa shell --endpoints endpoints.local.yml  --log-file out.log --credentials credentials.yml 
```

Para **depurar problemas**, utilizamos el siguiente comando que guarda los logs en `out.log` y muestra los registros con el flag `--debug`:

```zsh
rasa shell --endpoints endpoints.local.yml --log-file out.log --credentials credentials.yml --debug
```

 También se puede utilizar `rasa interactive` para depurar y añadir datos o historias de forma interactiva.

 ```zsh
 rasa interactive --endpoints endpoints.local.yml
 ```

## Integraciones

### Telegram

Para conectar el bot con Telegram es necesario crear un nuevo bot con [BotFather](https://core.telegram.org/bots/features#botfather). Al crearlo, nos pedirá el `username` y nos proporcionará el `TELEGRAM_API_TOKEN` que tendremos que copiar en `.env`.  

## Exponer al exterior

Para exponer el bot y poder conectar a un canal externo, como Telegram, es necesario correr el siguiente comando:

```zsh
ngrok http http://127.0.0.1:5005
```

Copiar la IP de Forwarding que nos proporciona [ngrok](https://ngrok.com/download) en `.env`. 

**Al utilizar docker, es necesario eliminar el volumen antiguo:** 

```zsh
docker rm rasa_server
docker volume rm bot-to-gancio_models
```

Volver a levantar en local, docker o con docker compose. Si quieres levantarlo una vez para probarlo, la forma más rápida será mediante docker o docker-compose. Si estás desarrollando o probando nuevas funcionalidades, lo más rápido será probarlo en local. Para **depurar problemas**, se puede utilizar el comando `rasa shell` con el flag **`--verbose`**, aunque no se contectará con los canales integrados. También se puede utilizar `rasa interactive` para añadir datos e historias de forma interactiva.

```zsh
# Local
export $(xargs <.env.local)
rasa run actions --port 5055;
rasa run --model models --endpoints endpoints.local.yml --credentials credentials.yml
rasa shell --endpoints endpoints.local.yml --log-file out.log --credentials credentials.yml --verbose
rasa interactive --endpoints endpoints.local.yml

# Docker
docker run --net gancio_default -v $(pwd)/actions:/app/actions --name action-server-docker  -it $(docker build -q ./actions)
docker run -it --net gancio_default -v $(pwd):/app --name rasa-docker --env-file .env rasa/rasa:3.5.10-full shell

# Docker compose
docker compose up --build --force-recreate --remove-orphans
```

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[build-shield]: https://framagit.org/hacklab/bot-to-gancio/badges/main/pipeline.svg
[build-url]: https://framagit.org/hacklab/bot-to-gancio/-/pipelines
[license-url]: https://framagit.org/hacklab/bot-to-gancio/blob/master/LICENSE.txt
[license-shield]: https://img.shields.io/badge/license-AGPL-green.svg
[issues-url]: https://framagit.org/hacklab/bot-to-gancio/-/issues
[issues-shield]: https://img.shields.io/gitlab/issues/open/hacklab/bot-to-gancio?gitlab_url=https%3A%2F%2Fframagit.org